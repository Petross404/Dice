//
//  main.cpp
//  Dice
//
//  Created by Petros on 20/12/2017.
//  Copyright © 2017 Petros. All rights reserved.
//
#include "dice.hpp"
#include <ncurses.h>
#include <unistd.h>

int main(int argc, const char * argv[]) {
    Dice zari;
    int x = 0, y = 0;
    initscr();
    noecho();
    keypad(stdscr, TRUE);
    
    printw("%d", zari.get_number());
    refresh();
    
    int ch = getch();
    for (;;)
    {
        if (ch == 'R' || ch == 'r')
        {
            zari.roll_dice();
            clear();
            printw("%d", zari.get_number());
            refresh();
        }
        else if (ch == 'Q' || ch == 'q')
        {
            clear();
            refresh();
            endwin();
            break;
        }
        ch = getch();
    }
    return 0;
}
