//
//  dice.cpp
//  Dice
//
//  Created by Petros on 20/12/2017.
//  Copyright © 2017 Petros. All rights reserved.
//

#include "dice.hpp"

Dice::Dice(int num) : m_number(num){};

Dice::Dice()
{
    std::random_device rd;
    std::mt19937 e2(rd());
    std::uniform_int_distribution<int> dis(0,6);
    m_number = dis(e2);
}

int Dice::get_number()
{
    if ((this->m_number < 0) && (this->m_number < 6))
    {
        std::cerr << "Abort" << std::endl;
        exit(-1);
    }
    else
    {
        return this->m_number;
    }
}

void Dice::set_number(int num)
{
    this->m_number = num;
}

void Dice::roll_dice()
{
    std::random_device rd;
    std::mt19937 e2(rd());
    std::uniform_int_distribution<int> dis(1,6);
    m_number = dis(e2);
}

void Dice::show_dice()
{
    std::cout << " _________" << "\n";
    std::cout << "|         |" << "\n";
    std::cout << "|         |" << "\n";
}





