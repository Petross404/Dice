//
//  dice.hpp
//  Dice
//
//  Created by Petros on 20/12/2017.
//  Copyright © 2017 Petros. All rights reserved.
//

#ifndef dice_hpp
#define dice_hpp

#include <random>
#include <iostream>

class Dice
{
private:
    int m_number;
    
public:
    Dice(int num);
    Dice();
    int get_number();
    void set_number(int num);
    void roll_dice();
    void show_dice();
};
#endif /* dice_hpp */
